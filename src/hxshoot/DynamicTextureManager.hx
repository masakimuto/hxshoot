package hxshoot;
import js.Browser;
import js.html.CanvasElement;

/**
 * ...
 * @author masaki
 */
class DynamicTextureManager extends AssetManagerBase<CanvasElement>{
	public static var texture(default, null) : DynamicTextureManager;
	
	public function new() {
		texture = this;
		super();
		createTitle();
	}
	
	override function load(name:String) : CanvasElement {
		var elm : CanvasElement = cast Browser.document.createElement("canvas");
		items.set(name, elm);
		
		return elm;
	}
	
	function createTitle() {
		var elm = load("title");
		var w : Int = cast Canvas.Width - Canvas.Height * 3 / 4;
		var h = Canvas.Height;
		var r = w * .4;
		elm.width = w;
		elm.height = h;
		var c = elm.getContext2d();
		c.fillStyle = "#ffffff";
		c.fillRect(0, 0, elm.width, elm.height);
		c.strokeStyle = "#000000";
		c.lineWidth = 5;
		c.arc(elm.width / 2, elm.height / 2, r, 0, Math.PI * 2, false);
		
		c.stroke();
		c.fillStyle = "#000000";
		c.textAlign = "center";
		c.textBaseline = "middle";
		c.font = r * 1.2 + "px san-serif";
		c.fillText("M'", w / 2, h / 2);
	}
	
	override function itemReady(item:CanvasElement):Bool {
		return true;
	}
	
}