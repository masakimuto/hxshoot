package hxshoot;
import hxshoot.Input.KeyCode;
import js.Browser;
import js.html.KeyboardEvent;
import js.html.svg.FEConvolveMatrixElement;
import js.Lib;

/**
 * ...
 * @author masaki
 */
class Button {
	public var Count(default, null) : Int;
	public var Number (default, null) : Int;
	public var code (default, null) : KeyCode;
	
	public function new(number : Int) {
		Number = number;
		Count = 0;
	}
	
	public function push(x : Bool): Void {
		if (x) {
			Count++;
		}
		else {
			Count = 0;
		}
	}
}

enum KeyCode {
	Z;
	X;
	Up;
	Down;
	Left;
	Right;
	Esc;
}
 
class Input{

	public static var input (default, null) : Input;
	
	var keys : Map<KeyCode, Button>;
	var state : Map<Int, Bool>;
	static var keyCode : Map<KeyCode, Int>;
	
	public function new() {
		keyCode = new Map<KeyCode, Int>();
		keyCode.set(Z, 90);
		keyCode.set(X, 88);
		keyCode.set(Up, 38);
		keyCode.set(Down, 40);
		keyCode.set(Left, 37);
		keyCode.set(Right, 39);
		keyCode.set(Esc, 27);
		
		input = this;
		
		state  = new Map<Int, Bool>();
		keys = new Map<KeyCode, Button>();
		for (k in keyCode.keys()) {
			keys.set(k, new Button(keyCode.get(k)));
		}
		Browser.window.onkeydown = keydown;
		Browser.window.onkeyup = keyup;
		//Browser.window.addEventListener("keydown", keydown, true);
		//Browser.window.addEventListener("keyup", keyup, true);
	}
	
	
	function keydown(e : KeyboardEvent) {
		state.set(e.keyCode, true);
	}
	
	function keyup(e : KeyboardEvent) {
		state.set(e.keyCode, false);
	}
	
	public function update(): Void {
		for (k in keys) {
			k.push(state.get(k.Number) == true);
		}
	}
	
	public function getPush(key : KeyCode) : Bool {
		return keys.get(key).Count > 0;
	}
	
	public function getJust(key : KeyCode) : Bool {
		return keys.get(key).Count == 1;
	}
	
	public function getDirection() : Vector {
		var x = 0;
		var y = 0;
		if (getPush(Up)) y--;
		if (getPush(Down)) y++;
		if (getPush(Left)) x--;
		if (getPush(Right)) x++;
		if (x == 0 && y == 0) {
			return new Vector(0, 0);
		}
		return new Vector(x, y).norm();
	}
	
}