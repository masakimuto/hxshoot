package hxshoot;
import js.html.EventListener;
import js.html.Image;
import js.html.ImageElement;

/**
 * ...
 * @author masaki
 */
class TextureManager extends AssetManagerBase<ImageElement>{
	public static var texture(default, null) : TextureManager;
	
	public function new() {
		texture = this;
		super();
		loads(["ship.png", "wenemy.png", "small_r.png", "small_b.png"]);
	}
	
	override function load(name : String) : ImageElement {
		var i = new Image();
		
		i.src = "img/" + name;
		items.set(name, i);
		return i;
	}
	
	override function itemReady(item:ImageElement):Bool {
		return item.complete;
	}
	
}