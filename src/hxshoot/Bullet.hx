package hxshoot;

/**
 * ...
 * @author masaki
 */
@:keepSub class Bullet extends Character{

	public function setColor(red : Bool) {
		if (!red) {
			setImage("small_b.png");
		}
	}
	
	public function new(pos : Vector, vel : Vector) {
		super();
		setImage("small_r.png");
		position = pos;
		velocity = vel;
		radius = 6;
	}
	
	public function canRemove() : Bool {
		return !inBound(-30);
	}
	
}

