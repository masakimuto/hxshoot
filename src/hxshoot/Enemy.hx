package hxshoot;
import haxe.Constraints.Function;

/**
 * ...
 * @author masaki
 */
class Enemy extends Character{
	var count : Int;
	var playPattern : Void -> Void;
	
	public function new() {
		super();
		position = new Vector(Canvas.PlayWidth / 2, 100);
		velocity = new Vector(0, 0);
		setImage("wenemy.png");
		radius = 16;
		count = 0;
	}
	
	public function init(s : String) {
		playPattern = Reflect.field(this, s);
	}
	
	override public function update() {
		super.update();
		count++;
		if(playPattern != null){
			playPattern();
		}
	}
	
	function fire(spd : Float, angle : Float, red : Bool) {
		GameScene.game.bullets.set(position, Vector.fromPolar(spd, angle)).setColor(red);
		playSound("fire");
	}
	
	function fireWay(spd : Float, angle : Float, way : Int, width : Float, red : Bool) {
		angle -= width / 2;
		for (i in 0...way) {
			fire(spd, angle, red);
			angle += width / (way - 1);
		}
	}
	
	function fireAll(spd : Float, angle : Float, way : Int, red : Bool) {
		if (way % 2 == 0) {
			angle += (Math.PI / way);
		}
		for (i in 0...way) {
			fire(spd, angle, red);
			angle += Math.PI * 2 / way;
		}
	}
	
	function getShip() : Float {
		return getShipPosition().sub(position).getAngle();
	}
	
	function getShipPosition() : Vector {
		var s = GameScene.game.ship;
		if (s != null) {
			return s.position;
		}else {
			return Ship.Initial;
		}
	}
	
	function fireLine(max : Float, angle : Float, num : Int, min : Float, red : Bool) {
		var s = min;
		var d = Math.pow(max / min, 1.0 / num);
		for (i in 0...num) {
			fire(s, angle, red);
			s *= d;
		}
	}
	
	function patternAim() {
		if (count % 5 == 0) {
			fire(5, getShip(), true);
		}
	}
	
	function patternBasic1() {
		if (count % 10 == 0) {
			fireAll(14, count * 1.2, 10, false);
			fireAll(8, count * 1.1, 22, false);
		}
		if (count % 25 == 0) {
			fireLine(20, getShip(), 8, 2, true);
		}
		
	}
	
	/*function patternTest() {
		fireAll(10, 10, 2);
	}*/
	
	function patternChase() {
		var f = 90;
		var t0 = 15;
		if (count % f == 0) {
			velocity = new Vector(0, 0);
		}
		if (count % f == t0) {
			var dist = position.sub(getShipPosition()).length();
			velocity = Vector.fromPolar(dist/ (f - t0), getShip());
		}
		if (count % f > t0) {
			if (count % 3 == 0) {
				var sp = (count % f) / 8; 
				fire(6 + sp, getShip(), true);
			}
			if (count % 3 == 0) {
				var a : Float = 0;
				switch(Math.round(count / f + 0.5) % 4) {
					case 0:
						a = 0;
					case 1:
						a = Math.PI / 2;
					case 2:
						a = Math.PI;
					case 3:
						a = Math.PI * 1.5;
				}
				if (count % 2 == 0) {
					fire(1, a, false);
				}
				else {
					fire(3, a, false);
				}
			}
		}
	}
}