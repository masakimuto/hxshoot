package hxshoot;


/**
 * ...
 * @author masaki
 */
class StageSelecter {
	public static var stage(default, null) : StageSelecter;
	
	var stages : Array<String>;
	var selection = 0;
	
	public function new() {
		stages = [];
		var e : Dynamic = new Enemy();
		for (i in  Reflect.fields(e.__proto__)) {
			if(i.substr(0, 7) == "pattern") stages.insert(0, i.substr(7));
		}
		selectStage(false);
	}
	
	public function update() {
		if (Input.input.getJust(Up)) {
			selection--;
			if (selection < 0) {
				selection += stages.length;
			}
			selectStage(false);
		}
		if (Input.input.getJust(Down)) {
			selection++;
			if (selection >= stages.length) {
				selection -= stages.length;
			}
			selectStage(false);
		}
		if (Input.input.getJust(Z)) {
			selectStage(true);
		}
	}
	
	function selectStage(start : Bool) {
		var name = "pattern" + stages[selection % stages.length];
		if (start) {
			Main.global.startGame(name);
		}
		else {
			Main.global.previewGame(name);
		}
	}
	
	function calcY(i : Int) : Float {
		return 10 + i * 31;
	}
	
	
	
	public function draw() {
		var c = Canvas.canvas.getContext();
		var w = Canvas.Width - Canvas.PlayWidth;
		var x = Canvas.canvas.playOffset - w;
		var center = 7;
		c.fillStyle = "#ffffff";
		c.fillRect(x, 0, w, Canvas.Height);
		c.font = "20px sans-serif";
		c.textBaseline = "top";
		c.textAlign = "end";
		
		c.fillStyle = "#ff0000";
		c.fillRect(x, calcY(center), w, 31);
		
		c.fillStyle = "#000000";
		for (i in 0...15) {
			c.fillText(stages[(i + selection - center + stages.length * 100) % stages.length], x + w - 10 + (Math.abs(i - center) - center) * 0 , calcY(i));
		}
	}
	
}