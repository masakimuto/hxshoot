package hxshoot;

/**
 * ...
 * @author masaki
 */
class Vector{
	public var x : Float;
	public var y : Float;
	
	public function new(x : Float, y : Float) {
		this.x = x;
		this.y = y;
	}
	
	public function add(v : Vector) : Vector {
		return new Vector(x + v.x, y + v.y);
	}
	
	public function neg() : Vector {
		return new Vector( -x, -y);
	}
	
	public function sub(v : Vector) : Vector {
		return new Vector(x - v.x, y - v.y);
	}
	
	public function mul(a : Float) : Vector {
		return new Vector(x * a, y * a);
	}
	
	public function length() : Float {
		return Math.sqrt(x * x + y * y);
	}
	
	public function lengthSquared() : Float {
		return x * x + y * y;
	}
	
	public function norm(): Vector {
		return mul(1 / length());
	}
	
	public static function fromPolar(len : Float, angle : Float) : Vector {
		return new Vector(Math.cos(angle) * len, Math.sin(angle) * len);
	}
	
	public function getAngle() : Float {
		return Math.atan2(y, x);
	}
	
}