package hxshoot;
import js.html.Audio;
import js.html.AudioElement;
import js.html.MediaElement;

/**
 * ...
 * @author masaki
 */
class AudioManager extends AssetManagerBase<AudioElement>{
	public static var audio(default, null) : AudioManager;
	
	
	public function new() {
		audio = this;
		super();
		loads(["dest.wav", "fire.wav"]);
		
	}
	
	override function load(name : String) : AudioElement {
		var a = new Audio();
		a.src = "sound/" + name;
		items.set(name, a);
		return a;
	}
	
	override function itemReady(item:AudioElement):Bool {
		return item.readyState == 4;
	}
	
	public function play(name : String) {
		//return;
		var i = items.get(name + ".wav");
		if (!i.ended) {
			i.currentTime = 0;
			i.play();
		}
		else {
			i.play();	
		}
		
	}
	
}
