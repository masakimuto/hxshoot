package hxshoot;

/**
 * ...
 * @author masaki
 */
class GameScene{
	public static var game(default, null) : GameScene;
	
	public var ship(default, null) : Ship;
	public var enemy(default, null) : Enemy;
	public var bullets(default, null) : BulletManager;
	public var playing(default, null) : Bool;
	
	public function new(name : String, start : Bool) {
		game = this;
		playing = start;
		if (start) {
			ship = new Ship();	
		}
		//enemy = new TestEnemy();
		//enemy = Type.createInstance(Type.resolveClass("hxshoot.enemy." + name), []);
		enemy = new Enemy();
		enemy.init(name);
		bullets = new BulletManager();
	}
	
	
	
	public function update() {
		if (playing) {
			ship.update();	
		}
		enemy.update();
		bullets.update();
	}
	
	public function draw() {
		if (playing) {
			ship.draw();
		}
		enemy.draw();
		bullets.draw();
	}
	
}