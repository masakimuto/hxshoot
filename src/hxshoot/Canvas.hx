package hxshoot;


import js.html.CanvasElement;
import js.html.CanvasRenderingContext2D;
import js.html.Image;
import js.html.ImageElement;
import js.Lib;

/**
 * ...
 * @author masaki
 */
class Canvas{
	var element : CanvasElement;
	public static var Width(default, null) = 640;
	public static var Height(default, null) = 480;
	public static var PlayWidth(default, null) = Height * 3 / 4;
	public static var canvas(default, null) : Canvas;
	
	public var playOffset(default, null) : Float;
	
	var moveDir = 0;
	var moveCount = 0;
	var MoveTime(default, null) = 10;
	
	public function new(elm : CanvasElement) {
		canvas = this;
		this.element = elm;
		element.width = Width;
		element.height = Height;
		element.setAttribute("style", "border:1px solid gray");
		playOffset = Width - PlayWidth;
	}
	
	public function update() : Void {
		//playOffset += 1;
		if (moveCount != 0) {
			moveCount--;
			playOffset += (Width - PlayWidth) / MoveTime * moveDir;
		}
		var c = element.getContext2d();
		c.fillStyle = "#000000";
		c.fillRect(0, 0, Width, Height);
	}
	
	public function move(toLeft : Bool) {
		if (toLeft) {
			moveDir = 1;
		}else {
			moveDir = -1;
		}
		moveCount = MoveTime;
	}
	
	public function isMoving() : Bool {
		return moveCount != 0;
	}
	
	public function drawString(text : String, x : Float, y :Float, absolute : Bool = false) : Void {
		var c = element.getContext2d();
		c.fillStyle = "#ffffffff";
		if (!absolute) {
			x += playOffset;
		}
		c.fillText(text, x, y);
	}
	
	public function drawImage(img : ImageElement, pos : Vector, absolute : Bool = false) {
		var c = element.getContext2d();
		c.drawImage(img, pos.x + (absolute ? 0 : playOffset), pos.y);
	}
	
	public function drawCanvas(img : CanvasElement, pos : Vector, absolute : Bool = false) {
		var c = element.getContext2d();
		c.drawImage(img, pos.x + (absolute ? 0 : playOffset), pos.y);
	}
	
	public function getContext() : CanvasRenderingContext2D {
		return element.getContext2d();
	}
	
}