package hxshoot;

/**
 * ...
 * @author masaki
 */
class BulletManager {
	
	public var items(default, null) : Array<Bullet>;
	
	public function new() {
		items = new Array<Bullet>();	
	}
	
	public function set(pos : Vector, vel : Vector) : Bullet {
		var b = new Bullet(pos, vel);
		items.push(b);
		return b;
	}
	
	public function update() {
		var remove = new Array<Bullet>();
		for (i in items) {
			i.update();
			if (i.canRemove()) {
				remove.push(i);
			}
		}
		for (i in remove) {
			items.remove(i);
		}
		
	}
	
	public function draw() {
		for (i in items) {
			i.draw();
		}
	}
}