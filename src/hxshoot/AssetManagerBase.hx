package hxshoot;
import js.html.Element;
import js.html.sql.Exception;

/**
 * ...
 * @author masaki
 */
class AssetManagerBase<T: Element>{
	public var items(default, null) : Map<String, T>;
	
	public function new() {
		items = new Map<String, T>();
	}
	
	function loads(name : Array<String>) {
		for (i in name) {
			load(i);
		}
	}
	
	public function ready(): Bool {
		for (i in items) {
			if (!itemReady(i)) {
				//trace(i);
				return false;
			}
		}
		return true;
	}
	
	function itemReady(item : T): Bool {
		return false;
	}
	
	function load(name : String) : T {
		return null;
	}
	
}