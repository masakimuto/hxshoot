package hxshoot;
import js.html.ImageElement;


/**
 * ...
 * @author masaki
 */
class Character{
	var image : ImageElement;
	var position : Vector;
	var velocity : Vector;
	var radius :  Float;
	var size : Vector;
	
	public function new() {
		
	}
	
	public function update() {
		position = position.add(velocity);
	}
	
	function setImage(name : String) {
		image = TextureManager.texture.items.get(name);
		size = new Vector(image.width / 2, image.height / 2);
	}
	
	function inBound(offset : Float) : Bool {
		return position.x - offset > 0 && position.x + offset < Canvas.PlayWidth 
		&& position.y - offset > 0 && position.y + offset < Canvas.Height;
	}
	
	public function draw() : Void {
		Canvas.canvas.drawImage(image, position.sub(size));
	}
	
	function collision(t : Character) : Bool {
		return (position.sub(t.position)).lengthSquared() < (radius + t.radius) * (radius  + t.radius);
	}
	
	function playSound(name : String) {
		if(GameScene.game.playing) AudioManager.audio.play(name);
	}
	
}