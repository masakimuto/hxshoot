package hxshoot;

import js.Browser;
import js.html.Console;
import js.html.webgl.Texture;
import js.Lib;


/**
 * ...
 * @author masaki
 */

class Main {
	
	static function main() {
		var m = new Main();
		
	}
	
	public static var global(default, null) : Main;
	
	var timeout : Int;
	var canvas : Canvas;
	var input : Input;
	var ship : Ship;
	var enemy : Enemy;
	var bullets : BulletManager;
	var audio : AudioManager;
	var image : TextureManager;
	var dynamicImage : DynamicTextureManager;
	var stage : StageSelecter;
	var loadToken : Int;
	var game : GameScene;
	var playing : Bool;
	
	public function new() {
		global = this;
		canvas = new Canvas(cast Browser.document.getElementById("canvas"));
		audio = new AudioManager();
		image = new TextureManager();
		dynamicImage = new DynamicTextureManager();
		input = new Input();
		canvas.drawString("loading", 10, 10, true);
		loadToken = Browser.window.setInterval(function() {
				if (image.ready() && audio.ready()) {
					init();
					Browser.window.clearTimeout(loadToken);
				}
			}, 500);
	}
	
	function init() {
		playing = false;
		stage = new StageSelecter();
		start();
	}
	
	public function startGame(name : String) {
		if (playing) {
			return;
		}
		game = new GameScene(name, true);
		playing = true;
		canvas.move(false);
	}
	
	public function endGame() {
		if (!playing) {
			return;
		}
		playing = false;
		canvas.move(true);
	}
	
	public function previewGame(name : String) {
		game = new GameScene(name, false);
	}
	
	function update() : Void {
		input.update();
		game.update();	
		if(!playing && !canvas.isMoving()){
			stage.update();	
		}
		draw();
	}
	
	function draw() {
		canvas.update();
		game.draw();
		stage.draw();
		canvas.drawCanvas(dynamicImage.items.get("title"), new Vector(Canvas.PlayWidth, 0));
	}
	
	function start() : Void {
		timeout = Browser.window.setInterval(update, 33);
	}
	
	function end() : Void {
		Browser.window.clearTimeout(timeout);
	}
	
}