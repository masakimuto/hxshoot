package hxshoot;

/**
 * ...
 * @author masaki
 */
class Ship extends Character{
	
	var dead = false;
	public static var Initial = new Vector(Canvas.PlayWidth * 0.5, Canvas.Height * 0.8);
	
	public function new() {
		super();
		position = Initial;
		setImage("ship.png");
		radius = 3;
	}
	
	public override function update() {
		if (dead) return;
		
		var speed = Input.input.getPush(Z) ? 4 : 8;
		velocity = Input.input.getDirection().mul(speed);
		super.update();
		clamp();
		for (b in GameScene.game.bullets.items) {
			if (collision(b)) {
				playSound("dest");
				Main.global.endGame();
				dead = true;
				break;
			}
		}
	}
	
	function clamp() {
		var offset = 16;
		if (position.x < offset) {
			position.x = offset;
		}
		if (position.x > Canvas.PlayWidth - offset) {
			position.x = Canvas.PlayWidth - offset;
		}
		if (position.y < offset) {
			position.y = offset;
		}
		if (position.y > Canvas.Height - offset) {
			position.y = Canvas.Height - offset;
		}
		
	}
	
}